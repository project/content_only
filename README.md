INTRODUCTION
============

This lightweight module allows a user to render only the content area of a theme
if the URL contains the "_contentonly" parameter.
example : "https://mysite/my-page?_contentonly"


REQUIREMENTS
============

This module has no requirements.


INSTALLATION
============

Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.
